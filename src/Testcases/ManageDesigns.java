package Testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class ManageDesigns {

	public static WebDriver	driver = new FirefoxDriver();
	public static String downloadPath = Constant.Path_Downloads;
	private static WebElement element = null;
	private static String designName;

	private static String Workbooks;
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");	
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	/*
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	 */
	@Test
	public void Managedesigns_Filters() throws Exception 
	{
		System.out.println("Managedesigns_Filters Start");
		System.out.println("---------------------------");
		for (int i = 1; i < 2; i++) {

			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");
			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");

			// Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// Design ID
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement element = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[1]"));
			String strng = element.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			// date
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).click();
			driver.findElement(By.xpath("html/body/div[3]/div[1]/ul/li[5]")).click();
			Thread.sleep(3000);

			// Design Name
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement element1 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[3]/div"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(strng1);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement element11 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[4]/div"));
			String strng11 = element11.getText();			
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(strng11);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			/* dropdown */
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement mySelectElm_inactive = driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/select"));
			Select mySelect1 = new Select(mySelectElm_inactive);
			mySelect1.selectByVisibleText("inactive");
			Thread.sleep(9000);

			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement mySelectElm_active = driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/select"));
			Select mySelect2 = new Select(mySelectElm_active);
			mySelect2.selectByVisibleText("active");
			Thread.sleep(9000);

			driver.findElement(By.xpath(".//*[@id='filter-row']/th[6]/button")).click();
			WebElement mySelectElm_All_status = driver.findElement(By
					.xpath(".//*[@id='filter-row']/th[5]/select"));
			Select mySelect3 = new Select(mySelectElm_All_status);
			mySelect3.selectByVisibleText("All Status");
			Thread.sleep(9000);
			LogOut_Page.logout(driver);
		}
	}

	@Test
	public void Managedesigns_show() throws Exception
	{
		System.out.println("Managedesigns_show Start");
		System.out.println("-------------------------------------");
		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");

			// show option and back
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[1]")).click();
			Thread.sleep(9000);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div[1]/div/a[2]/span")).click();
			Thread.sleep(9000);
			LogOut_Page.logout(driver);
		}
	}


	@Test
	public void Managedesigns_Edit() throws Exception
	{
		System.out.println("Managedesigns_Edit Start");
		System.out.println("-------------------------------------");
		for (int i = 1; i < 2; i++) 
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");

			// Edit the design created
			Thread.sleep(3000);
			driver.findElement(	By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[3]/span")).click();
			String Field7 = ExcelUtils.getCellData(i, 3);
			driver.findElement(By.name("invd_f7_label")).clear();
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			// Back button
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div[2]/div[1]/div/a[2]/span")).click();
			Thread.sleep(3000);
			LogOut_Page.logout(driver);
		}

	}

	@Test
	public void Managedesigns_MandatoryFields() throws Exception
	{
		System.out.println("Managedesigns_MandatoryFields Start");
		System.out.println("-------------------------------------");
		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");
			Thread.sleep(2000);
			// Edit the design created
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[3]/span")).click();
			Thread.sleep(3000);
			// Clear the data
			driver.findElement(By.name("invd_l[1]")).clear();

			Thread.sleep(7000);
			// update button
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if (driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Item 1 Name field is required.")) 
			{
				System.out.println(" pass:The Item 1 Name field is required.");
			}
				else
				{
					System.out.println("Fail:The Item 1 Name field is required.");
					Thread.sleep(3000);
				}
					LogOut_Page.logout(driver);
					
					}
	}


	@Test
	public void Managedesigns_exceldownload() throws Exception
	{

		System.out.println("---Managedesigns_exceldownload---");
		System.out.println("-------------------------------------");
		WebDriver driver = new FirefoxDriver(FirefoxDriverProfile1());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);
		driver.findElement(By.name("busi_login")).sendKeys("shravya.vanka06@gmail.com");
		driver.findElement(By.name("password")).sendKeys("asha8050");
		driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(Xpath.managedesign)).click();
		
		driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[6]/a[2]/span")).click(); 
		LogOut_Page.logout(driver); 
	}
	public static FirefoxProfile FirefoxDriverProfile1() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browsser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;



	}

	@Test
	public void Managedesigns_add_option() throws Exception {

		System.out.println("-------------------------------------");
		System.out.println("Managedesigns_add_option Start");
		System.out.println("-------------------------------------");
		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");
			// add button goes to creat design
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName = ExcelUtils.getCellData(i, 6) + formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i, 7);
			String Field1 = ExcelUtils.getCellData(i, 8);
			String Field2 = ExcelUtils.getCellData(i, 9);
			String Field3 = ExcelUtils.getCellData(i, 10);
			String Field4 = ExcelUtils.getCellData(i, 11);
			String Field5 = ExcelUtils.getCellData(i, 12);
			String Field6 = ExcelUtils.getCellData(i, 13);
			String Field7 = ExcelUtils.getCellData(i, 14);
			String Field8 = ExcelUtils.getCellData(i, 15);
			String Itemname1 = ExcelUtils.getCellData(i, 16);
			String Tax = ExcelUtils.getCellData(i, 17);
			String servicetax = ExcelUtils.getCellData(i, 18);

			// Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " + Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div[1]/div/a[2]")).click();
			Thread.sleep(6000);
			LogOut_Page.logout(driver);

		}
	}


	@Test
	public void Managedesigns_Read_Excel_custom_line_item() throws IOException {
		System.out.println("-----------------------------------------");
		System.out.println("Managedesigns_Read_Excel_custom_line_item");
		System.out.println("-----------------------------------------");
		String excelFilePath = "D://DownloadFiles//excel_file_dak_custom_line_items.xlsx";
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					System.out.print(cell.getStringCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					System.out.print(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_NUMERIC:
					System.out.print(cell.getNumericCellValue());
					break;
				}
				System.out.print("");
			}
			System.out.println();
		} 
		workbook.close();
		inputStream.close();
	}



	@Test
	public  void Managedesigns_exceldownload_customer_lineitem() throws Exception
	{
		//driver.get(Constant.URL);
		System.out.println("Managedesigns_exceldownload_customer_lineitem Start");
		System.out.println("-------------------------------------");
		WebDriver driver = new FirefoxDriver(FirefoxDriverProfile1());	
		driver.manage().window().maximize();
		driver.get(Constant.URL);
		driver.findElement(By.name("busi_login")).sendKeys("shravya.vanka06@gmail.com");
		driver.findElement(By.name("password")).sendKeys("asha8050");
		driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath(Xpath.managedesign)).click();
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[6]/span")).click(); 
		Thread.sleep(4000);
		LogOut_Page.logout(driver); 
	}
	public static FirefoxProfile FirefoxDriverProfile() throws Exception {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", downloadPath);
		profile.setPreference("browser.helperApps.neverAsk.openFile","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");			
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		profile.setPreference("browser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		profile.setPreference("browser.download.manager.focusWhenStarting", false);
		profile.setPreference("browser.download.manager.useWindow", false);
		profile.setPreference("browser.download.manager.showAlertOnComplete", false);
		profile.setPreference("browser.download.manager.closeWhenDone", false);
		return profile;
	}

	@Test
	public  void Managedesigns_previous_next() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("Managedesigns_previous_next Start");
		System.out.println("-------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Managedesigns_1,"Sheet1");

			driver.findElement(By.xpath("html/body/div[2]/aside/section/ul/li[9]/a/span")).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay List Invoice Designs");  

			/*previous and next*/
			//2 
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
			Thread.sleep(3000);
			// 3
			driver.findElement(By.xpath(" .//*[@id='dataTableBuilder_paginate']/ul/li[4]/a")).click();
			Thread.sleep(3000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			//previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);

			//previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);

			LogOut_Page.logout(driver); 
		}
	}

	@Test
	public void Managedesigns_show_entries() throws Exception 
	{

		System.out.println("-------------------------------------");
		System.out.println("Managedesigns_show_entries Start");
		System.out.println("-------------------------------------");
		driver.get(Constant.URL); 
		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");

			/* dropdown */
			WebElement mySelectElm = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect = new Select(mySelectElm);
			mySelect.selectByVisibleText("10");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm2 = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect2 = new Select(mySelectElm);
			mySelect.selectByVisibleText("25");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm3 = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect3 = new Select(mySelectElm);
			mySelect.selectByVisibleText("50");
			Thread.sleep(9000);



			/* dropdown */
			WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
			Select mySelect4= new Select(mySelectElm);
			mySelect.selectByVisibleText("100");
			Thread.sleep(9000);

			LogOut_Page.logout(driver); 
		}

	}



	@Test
	public void Managedesigns_Delete() throws Exception

	{
		System.out.println("-------------------------------------");
		System.out.println("Managedesigns_Delete Start");
		System.out.println("-------------------------------------");

		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");
			/* dropdown */
			WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/select"));
			Select mySelect2 = new Select(mySelectElm2);
			mySelect2.selectByVisibleText("active");
			Thread.sleep(4000);

			driver.findElement(	By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[4]/span")).click();
			LogOut_Page.logout(driver);
		}
	}

	@Test
	public void Managedesigns_Delete_Recycle() throws Exception
	{
		System.out.println("-------------------------------------");
		System.out.println("Managedesigns_Delete_Recycle Start");
		System.out.println("-------------------------------------");
		for (int i = 1; i < 2; i++) {
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_LoginData, "Sheet1");
			System.out.println("loaded: " + i);
			String sUserName = ExcelUtils.getCellData(i, 1);
			String sPassword = ExcelUtils.getCellData(i, 2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword, driver, i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData+ Constant.File_Managedesigns_1, "Sheet1");

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			String expectedTitle = "TraknPay List Invoice Designs";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
			System.out.println("TraknPay List Invoice Designs");
			// Delete
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[4]/span")).click();
			// Recycle
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[6]/a[5]/span")).click();
			LogOut_Page.logout(driver);	
		} 
	}

	@Test
	public  void Managedesigns_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("Managedesigns session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.managedesign)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}


