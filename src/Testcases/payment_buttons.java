package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class payment_buttons {


	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element = null;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
	//	driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}


	@Test 
	public  void payment_buttons_filters() throws Exception

	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_filters");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


			// Payment Button Name
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[1]/div"));
			String strng = element.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(strng);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[1]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			// Amount
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element1 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[2]"));
			String strng1 = element1.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(strng1);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[2]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Payment Button Description	
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[3]/div"));
			String strng2 = element2.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(strng2);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[3]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			//Purpose of Payment	Action
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();
			WebElement element3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[4]/div"));
			String strng3 = element3.getText();
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(strng3);
			driver.findElement(By.xpath(".//*[@id='filter-row']/th[4]/input")).sendKeys(Keys.RETURN);
			Thread.sleep(3000);

			driver.findElement(By.xpath(".//*[@id='filter-row']/th[5]/button")).click();

		}
	}


	@Test 
	public  void payment_buttons_Delete() throws Exception
	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_Delete");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Delete
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]/a[2]/span")).click();
			driver.findElement(By.xpath("html/body/div[4]/div[7]/div/button")).click();
			Thread.sleep(3000);

		}
	}

	@Test 
	public  void payment_buttons_add() throws Exception

	{	

		System.out.println("--------------------------------");
		System.out.println("payment_buttons_add");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1"); 
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);

			// add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();
			String Payment_Button_Name = ExcelUtils.getCellData(i,3);
			String Payment_Button_Description = ExcelUtils.getCellData(i,4);
			String Amount = ExcelUtils.getCellData(i,5);
			String Purpose_Payment = ExcelUtils.getCellData(i,6);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//*[@id='payb_name']")).sendKeys(Payment_Button_Name);
			driver.findElement(By.xpath(".//*[@id='payb_description']")).sendKeys(Payment_Button_Description);   
			driver.findElement(By.xpath(".//*[@id='payb_amount']")).sendKeys(Amount);
			driver.findElement(By.xpath(".//*[@id='payb_purpose_of_payment']")).sendKeys(Purpose_Payment);
			Thread.sleep(3000);	
		}
	}

	@Test 
	public  void payment_buttons_add_mandatoryfields() throws Exception

	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_Mandatory Fields");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);

			// add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();
			Thread.sleep(2000);
			//String Payment_Button_Name = ExcelUtils.getCellData(i,3);
			String Payment_Button_Description = ExcelUtils.getCellData(i,4);
			String Amount = ExcelUtils.getCellData(i,5);
			String Purpose_Payment = ExcelUtils.getCellData(i,6);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//driver.findElement(By.xpath(".//*[@id='payb_name']")).sendKeys(Payment_Button_Name);
			driver.findElement(By.xpath(".//*[@id='payb_description']")).sendKeys(Payment_Button_Description);   
			driver.findElement(By.xpath(".//*[@id='payb_amount']")).sendKeys(Amount);
			driver.findElement(By.xpath(".//*[@id='payb_purpose_of_payment']")).sendKeys(Purpose_Payment);
			Thread.sleep(3000);	


			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[1]/div/p")).getText().equalsIgnoreCase("The Payment Name field is required."))
			{                            
				System.out.println("pass:The Payment Name field is required.");
			}
			else
			{
				System.out.println("Fail:The Payment Name field is required.");
			}
		}
	}



	@Test 
	public  void payment_buttons_add_Amount() throws Exception

	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_add_Amount");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);

			// add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[1]/div/a")).click();
			Thread.sleep(2000);

			String Payment_Button_Name = ExcelUtils.getCellData(i,3);
			String Payment_Button_Description = ExcelUtils.getCellData(i,4);
			String Amount = ExcelUtils.getCellData(i,9);
			String Purpose_Payment = ExcelUtils.getCellData(i,6);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//*[@id='payb_name']")).sendKeys(Payment_Button_Name);
			driver.findElement(By.xpath(".//*[@id='payb_description']")).sendKeys(Payment_Button_Description);   
			driver.findElement(By.xpath(".//*[@id='payb_amount']")).sendKeys(Amount);
			driver.findElement(By.xpath(".//*[@id='payb_purpose_of_payment']")).sendKeys(Purpose_Payment);
			Thread.sleep(3000);	


			//add
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();
			if(driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[1]/div[3]/div/p")).getText().equalsIgnoreCase("The Payment Amount must be a number."))
			{                            
				System.out.println(" Error:The Payment Amount must be a number.");
			}
			else
			{
				System.out.println("Fail:The Payment Amount must be a number.");
			}
		}
	}


	@Test 
	public  void payment_buttons_Edit() throws Exception

	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_Edit");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");     
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);

			// Edit 
			driver.findElement(	By.xpath(".//*[@id='dataTableBuilder']/tbody/tr[1]/td[5]/a[1]/span")).click();

			String Payment_Button_Description = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath(".//*[@id='payb_description']")).clear();
			driver.findElement(By.xpath(".//*[@id='payb_description']")).sendKeys(Payment_Button_Description);
			Thread.sleep(7000);

			//update
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/form/div[2]/div/button")).click();

			Thread.sleep(3000);

		}
	}

	@Test 
	public  void payment_buttons_show_entries() throws Exception
	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_show_entries");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);

			/* dropdown */
			WebElement mySelectElm = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect = new Select(mySelectElm);
			mySelect.selectByVisibleText("10");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm2 = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect2 = new Select(mySelectElm);
			mySelect.selectByVisibleText("25");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm3 = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect3 = new Select(mySelectElm);
			mySelect.selectByVisibleText("50");
			Thread.sleep(9000);

			/* dropdown */
			WebElement mySelectElm4 = driver
			.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select"));
			Select mySelect4 = new Select(mySelectElm);
			mySelect.selectByVisibleText("100");
			Thread.sleep(9000);
		}
	}

	@Test 
	public  void payment_buttons_previous_next() throws Exception
	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons_previous_next");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_payment_button,"Sheet1");
			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			Thread.sleep(2000);


			/* previous and next */
			/*
		// 2
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
		Thread.sleep(3000);

		// 3
		driver.findElement(By.xpath(" .//*[@id='dataTableBuilder_paginate']/ul/li[4]/a")).click();
		Thread.sleep(3000);

		// next
		driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
		Thread.sleep(4000);
			 */
			// next
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
			Thread.sleep(4000);

			// previous
			driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
			Thread.sleep(3000);
		}
	}


	@Test
	public  void payment_buttons_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("payment_buttons session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			driver.findElement(By.xpath(Xpath.payment_buttons)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}