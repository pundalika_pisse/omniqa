package Testcases;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogOut_Page;

public class user_login {
	
	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element = null;
	private WebElement clickAt;
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	/*
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
*/
	
	@Test
	public void user_login() throws Exception
	 {
		driver.get(Constant.URL);
	    System.out.println("user_login Start");
		 for(int i=1 ; i<2; i++ ) {
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
	           
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
	
		driver.findElement(By.name("busi_login")).sendKeys(sUserName);
		driver.findElement(By.name("password")).sendKeys(sPassword);
		driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
		    	
		Thread.sleep(6000);	   
		String expectedTitle = "TraknPay Dashboard";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle,actualTitle);
		System.out.println("TraknPay Dashboard");
		
		
		LogOut_Page.logout(driver);
		 }	
	 	}

@Test
public void user_login_mandatoryfields_Name() throws Exception
 {
	driver.get(Constant.URL);
    System.out.println("user_login_mandatoryfields_Name Start");
	 for(int i=1 ; i<2; i++ ) {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
           
	//String sUserName = ExcelUtils.getCellData(i,1);
	String sPassword = ExcelUtils.getCellData(i,2);

	//driver.findElement(By.name("busi_login")).sendKeys(sUserName);
	driver.findElement(By.name("password")).sendKeys(sPassword);
	driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
	    	
	Thread.sleep(3000);	   
	String expectedTitle = "TraknPay Home";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("TraknPay Home");
	if(driver.findElement(By.xpath("html/body/div/div[2]/form/div[1]/p")).getText().equalsIgnoreCase("The Username field is required."))
	   	
	   {
		    System.out.println(" The Username field is required.");
		}
		 }	
 	}

@Test
public void user_login_mandatoryfields_Password() throws Exception
 {
	driver.get(Constant.URL);
    System.out.println("user_login_mandatoryfields_Password Start");
	 for(int i=1 ; i<2; i++ ) {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
           
	String sUserName = ExcelUtils.getCellData(i,1);
	//String sPassword = ExcelUtils.getCellData(i,2);

	driver.findElement(By.name("busi_login")).sendKeys(sUserName);
	//driver.findElement(By.name("password")).sendKeys(sPassword);
	driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
	    	
	Thread.sleep(3000);	   
	String expectedTitle = "TraknPay Home";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("TraknPay Home");
	if(driver.findElement(By.xpath("html/body/div/div[2]/form/div[2]/p")).getText().equalsIgnoreCase("The Password field is required."))
	   	
	   {
		    System.out.println(" The Password field is required.");
		    }
		 }	
 	}

@Test
public void user_login_credentials_Name() throws Exception
 {
	driver.get(Constant.URL);
    System.out.println("user_login_credentials_name Start");
	 for(int i=1 ; i<2; i++ ) {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
           
	String sUserName = ExcelUtils.getCellData(i,6);
	String sPassword = ExcelUtils.getCellData(i,2);
		   

	driver.findElement(By.name("busi_login")).sendKeys(sUserName);
	driver.findElement(By.name("password")).sendKeys(sPassword);
	driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
	    	
	Thread.sleep(3000);	   
	String expectedTitle = "TraknPay Home";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("TraknPay Home");
	if(driver.findElement(By.xpath("html/body/div/div[2]/form/div[1]/p")).getText().equalsIgnoreCase("These credentials do not match our records."))
	   	
	   {
		    System.out.println("These credentials do not match our records.");
		    }
		 }	
 	}


@Test
public void user_login_credentials_Password() throws Exception
 {
	driver.get(Constant.URL);
    System.out.println("user_login_credentials_Password Start");
	 for(int i=1 ; i<2; i++ ) {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
           
	String sUserName = ExcelUtils.getCellData(i,1);
	String sPassword = ExcelUtils.getCellData(i,8);

	driver.findElement(By.name("busi_login")).sendKeys(sUserName);
	driver.findElement(By.name("password")).sendKeys(sPassword);
	driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
	    	
	Thread.sleep(3000);	   
	String expectedTitle = "TraknPay Home";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("TraknPay Home");
	if(driver.findElement(By.xpath("html/body/div/div[2]/form/div[1]/p")).getText().equalsIgnoreCase("These credentials do not match our records."))
		          {
		    System.out.println("These credentials do not match our records.");
				}
		 }	
 	}



@Test
public void user_login_forgot_password() throws Exception
 {
	driver.get(Constant.URL);
    System.out.println("user_login_forgot_Password Start");
	 for(int i=1 ; i<2; i++ ) {
		 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_New_login,"Sheet1");
           
	String sUserName = ExcelUtils.getCellData(i,11);
	String sPassword = ExcelUtils.getCellData(i,12);

	driver.findElement(By.name("busi_login")).sendKeys(sUserName);
	driver.findElement(By.name("password")).sendKeys(sPassword);
	driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();
	    	
	Thread.sleep(3000);	   
	String expectedTitle = "TraknPay Home";
	String actualTitle = driver.getTitle();
	Assert.assertEquals(expectedTitle,actualTitle);
	System.out.println("TraknPay Home");
	if(driver.findElement(By.xpath("html/body/div/div[2]/form/div[1]/p")).getText().equalsIgnoreCase("These credentials do not match our records."))
		          {
		    System.out.println("These credentials do not match our records.");
		 Thread.sleep(3000);
		 
  // Click on forgot the password
     driver.findElement(By.xpath("html/body/div/div[2]/form/div[4]/div[2]/a")).click();
     
     String EMailAddress = ExcelUtils.getCellData(i,10);
     driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
 //send password reset link
     driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/div/div[1]/button")).click();
     Thread.sleep(15000); 
     
     driver.get(Constant.URL2); 
       
 //gmail id and password
     String EMailAddress1 = ExcelUtils.getCellData(i,10);
     driver.findElement(By.name("Email")).sendKeys(EMailAddress1);
     //Next
     driver.findElement(By.name("signIn")).click();
     Thread.sleep(3000);
     
     String password = ExcelUtils.getCellData(i,13);
     driver.findElement(By.xpath(".//*[@id='Passwd']")).sendKeys(password);
     Thread.sleep(3000);
     //Sign In
     driver.findElement(By.xpath(".//*[@id='signIn']")).click();
     Thread.sleep(1000);
     //load on basic html          
    // driver.findElement(By.xpath(".//*[@id='stb']/input[2]")).click();
    
     //open the mail
  driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[2]/tbody/tr[1]/td[3]/a/span")).click();
                              
  //click on the link
 driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/table[4]/tbody/tr/td/table[1]/tbody/tr[4]/td/div/a")).click();
 Thread.sleep(3000);
 
     // open the mail
     clickAt=driver.findElement(By.xpath("div[5]/div/div/table/tbody/tr[1]"));
    
    //fields for new password
     String EMailAddress2 = ExcelUtils.getCellData(i,10);
     System.out.println("EMailAddress: "+EMailAddress2); 
    // driver.findElement(By.xpath(".//*[@id='busi_primary_email']")).sendKeys(EMailAddress2);
     driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress2);
                               //    .//*[@id='busi_primary_email']
     String password2 = ExcelUtils.getCellData(i,14);
     driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(password2);
     
     String RetypePassword = ExcelUtils.getCellData(i,14);
     driver.findElement(By.xpath(".//*[@id='password_confirmation']")).sendKeys(RetypePassword);
     
     //click on reset password
     driver.findElement(By.xpath("html/body/div/div[2]/form/div[6]/div/div[1]/button")).click();
   		 }	
     }
  }

}
