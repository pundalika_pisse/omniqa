package Testcases;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class email_sms_alert {
	
	
	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element= null;
	private static String CustomerMobile;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
	//	driver = new FirefoxDriver();
		driver.get(Constant.URLalert);
	}

	@Test
	public void payment_alerts() throws Exception
	{

		System.out.println("-----------------------------");
		System.out.println("payment email_SMS alert start");
		System.out.println("-----------------------------");
		for(int i=1 ; i<2; i++ ) {
			
			String expectedTitle = "TraknPay Generate Challan";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("TraknPay Generate Challan");  

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_email_sms,"Sheet1");
			
			//Define implict Wait           
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			String name = ExcelUtils.getCellData(i,1);
			driver.findElement(By.id("invd_name")).sendKeys(name);
			
			String mobile = ExcelUtils.getCellData(i,2);
			driver.findElement(By.id("invd_phone")).sendKeys(mobile);
			Thread.sleep(5000);
			
			String email = ExcelUtils.getCellData(i,3);
			driver.findElement(By.id("invd_email")).sendKeys(email);
			Thread.sleep(5000);
			
			String Purpose_payment = ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Purpose_payment);
			
			String amount = ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("invd_l_value[1]")).sendKeys(amount);	
		
			//click on generate
			driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/form/div[2]/div[2]/button")).click();
		        
			//click on confrim and pay
			driver.findElement(By.xpath("html/body/div[2]/div[3]/a[2]/strong")).click();
			
			String cardno1 = ExcelUtils.getCellData(i,7);
			String cardno2 = ExcelUtils.getCellData(i,8);
			String cardno3 = ExcelUtils.getCellData(i,9);
			String cardno4 = ExcelUtils.getCellData(i,10);
			String nameoncard= ExcelUtils.getCellData(i,6);
						
			driver.findElement(By.id("cc_number")).sendKeys(cardno1);
			driver.findElement(By.id("cc_number")).sendKeys(cardno2);
			driver.findElement(By.id("cc_number")).sendKeys(cardno3);
			driver.findElement(By.id("cc_number")).sendKeys(cardno4);
			//card name
			driver.findElement(By.id("cc_name")).sendKeys(nameoncard);
		

			//expiry date
			WebElement mySelectElm11 = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/form/div[3]/div[2]/div/div[1]/select")); 
			Select mySelect11= new Select(mySelectElm11);
			mySelect11.selectByValue("05");
			Thread.sleep(2000);

			WebElement mySelectElm111 = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/form/div[3]/div[2]/div/div[2]/select")); 
			Select mySelect111= new Select(mySelectElm111);
			mySelect111.selectByVisibleText("2023");
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/form/div[3]/div[2]/div/div[3]/input")).clear();
			String cardcvv = ExcelUtils.getCellData(i,11);
			driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/form/div[3]/div[2]/div/div[3]/input")).sendKeys(cardcvv);
			Thread.sleep(3000);
			
			//make payment
			driver.findElement(By.xpath("html/body/div[2]/div/button")).click();
			//email radio buttons
			driver.findElement(By.xpath("html/body/form/div/div/div[3]/div[3]/div/label/input")).click();
			//continue
			driver.findElement(By.xpath("html/body/form/div/div/div[3]/div[5]/input[1]")).click();
						
			driver.findElement(By.xpath("html/body/form/div/div/div[4]/ul[3]/li[2]/span/input")).clear();
			String password = ExcelUtils.getCellData(i,12);
			driver.findElement(By.xpath("html/body/form/div/div/div[4]/ul[3]/li[2]/span/input")).sendKeys(password);
			Thread.sleep(3000);
			
			//submit
			driver.findElement(By.xpath("html/body/form/div/div/div[4]/div[2]/input[1]")).click();
			Thread.sleep(8000);
			Thread.sleep(8000);
			Thread.sleep(8000);
			
			driver.get(Constant.URL2);
			
			//gmail id and password
		     String EMailAddress1 = ExcelUtils.getCellData(i,14);
		     driver.findElement(By.name("Email")).sendKeys(EMailAddress1);
		     //Next
		     driver.findElement(By.name("signIn")).click();
		     Thread.sleep(3000);
		     
		     String password2 = ExcelUtils.getCellData(i,13);
		     driver.findElement(By.xpath(".//*[@id='Passwd']")).sendKeys(password2);
		     Thread.sleep(3000);
		     //Sign In
		     driver.findElement(By.xpath(".//*[@id='signIn']")).click();
		     Thread.sleep(1000);
		     
		     //open the mail
		     driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[2]/tbody/tr[1]/td[3]/a/span")).click();
		                                 
		     //click on the link
		    driver.findElement(By.xpath("html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div[1]/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[7]/div/div[2]/div[2]/div[2]/table/tbody/tr[2]/td/div")).click();
		    Thread.sleep(3000);
		    
		    driver.findElement(By.xpath("html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div[1]/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[7]/div/div[2]/div[2]/div[2]/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/div/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td")).click();
		    Thread.sleep(3000);
		    
			
		}
		
	}
}
