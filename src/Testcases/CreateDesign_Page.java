package Testcases;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;

public class CreateDesign_Page {
	public static WebDriver	driver = new FirefoxDriver();

	private static WebElement element = null;
	private static String designName;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	@Test 
	public  void createdesign_create() throws Exception
	{	
		System.out.println("--------------------------------");
		System.out.println("createdesign_create Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;

			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);

			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);

			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);

			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);

			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);      

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			Thread.sleep(3000);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(1000);


		}

	}

	@Test 
	public void createdesign_edit() throws Exception
	{

		System.out.println("--------------------------------");
		System.out.println("createdesign_edit Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);		     

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");

			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);    
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(5000);

			//Edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div[1]/div/a[1]/span")).click();
			Field7 = ExcelUtils.getCellData(i,17);
			driver.findElement(By.name("invd_f7_label")).clear();
			driver.findElement(By.name("invd_f7_label")).sendKeys( Field7);
			Thread.sleep(7000);	 

			Itemname1 = ExcelUtils.getCellData(i,18);
			driver.findElement(By.name("invd_l[1]")).clear();
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			Thread.sleep(7000);
			driver.findElement(By.xpath(" html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(2000);			
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(2000);
		}
	}


	@Test 
	public void createdesign_skip_fields() throws Exception
	{

		System.out.println("--------------------------------");
		System.out.println("createdesign_skip_fields Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;

			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			//	String Field4 = ExcelUtils.getCellData(i,6);

			Thread.sleep(7000);

			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			//	String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String SubTotal = ExcelUtils.getCellData(i,13);
			//	String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);

			Thread.sleep(7000);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			//  driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			Thread.sleep(7000);
			//  driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			//  driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

		}
	}


	@Test 
	public void createdesign_skip_all_fields() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("createdesign_skip_all_fields Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();	     

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			Thread.sleep(9000);

			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String SubTotal = ExcelUtils.getCellData(i,12);
			String Tax = ExcelUtils.getCellData(i,13);
			String servicetax = ExcelUtils.getCellData(i,14);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);

			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");

			Thread.sleep(7000);
		}
	}


	@Test
	public void createdesign_remove_amount_fields() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("createdesign remove amount fields Start");
		System.out.println("-------------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);

			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			//String SubTotal = ExcelUtils.getCellData(i,13);
			//String Tax = ExcelUtils.getCellData(i,14);
			//String servicetax = ExcelUtils.getCellData(i,15);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			//  driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			//  driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			//  driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(4000);
		}
	}


	@Test
	public void createdesign_service_Tax_with_percentage() throws Exception
	{
		System.out.println("--------------------------------------------------------");
		System.out.println("createdesign service Tax with percentage Start");
		System.out.println("---------------------------------------------------------");

		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);

			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,23);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(9000);
		}
	}

	@Test 

	public void createdesign_designname_exists() throws Exception
	{
		System.out.println("--------------------------------");
		System.out.println("createdesign_design name exists Start");
		System.out.println("----------------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");


			String Designname = ExcelUtils.getCellData(i,1);
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);

			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);    
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li")).getText().equalsIgnoreCase("The Design Name has already been taken."))

			{
				System.out.println("Pass:The Design Name has already been taken.");
			}
			else
			{
				System.out.println("Fail:The Design Name has already been taken.");
			}
			Thread.sleep(9000);
		} 
	}

	@Test 
	public void createdesign_same_itemname() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_same_itemnameStart");
		System.out.println("---------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,26);
			String Itemname2 = ExcelUtils.getCellData(i,27);
			String SubTotal = ExcelUtils.getCellData(i,12);
			String Tax = ExcelUtils.getCellData(i,13);
			String servicetax = ExcelUtils.getCellData(i,14);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);    
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[1]/ul/li[1]")).getText().equalsIgnoreCase("The Item 1 Name cannot have same value as in Item 2 Name."))

			{
				System.out.println("Pass:The Item 1 Name cannot have same value as in Item 2 Name.");
			}
			else
			{
				System.out.println("Fail:The Item 1 Name cannot have same value as in Item 2 Name.");

			}
			Thread.sleep(9000);
		}
	}

	@Test 
	public void createdesign_back_option() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_back_option Start");
		System.out.println("--------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div[1]/div/a[2]/span")).click();
			Thread.sleep(9000);
		}				
	}


	@Test 
	public void createdesign_header_footer() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_header_footer Start");
		System.out.println("-------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);
			String NoteHere = ExcelUtils.getCellData(i,30);
			String Footer = ExcelUtils.getCellData(i,31);


			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_n1")).sendKeys(NoteHere);

			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			driver.findElement(By.name("invd_n2")).sendKeys(Footer);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(9000);
		}
	}


	@Test 
	public void createdesign_add_five() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_add_five Start");
		System.out.println("-----------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String Itemname3 = ExcelUtils.getCellData(i,33);
			String Itemname4 = ExcelUtils.getCellData(i,34);
			String Itemname5 = ExcelUtils.getCellData(i,35);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);
			String NoteHere = ExcelUtils.getCellData(i,30);
			String Footer = ExcelUtils.getCellData(i,31);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_n1")).sendKeys(NoteHere);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[3]")).sendKeys(Itemname3);
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[4]")).sendKeys(Itemname4);
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[5]")).sendKeys(Itemname5);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			driver.findElement(By.name("invd_n2")).sendKeys(Footer);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(9000);
		}
	}


	@Test 
	public  void createdesign_edit_all_fields() throws Exception
	{	
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_edit_all_fields Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();

			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");

			//Edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div[1]/div/a[1]/span")).click();
			Field1 = ExcelUtils.getCellData(i,37);
			driver.findElement(By.name("invd_f1_label")).clear();
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);
			Thread.sleep(4000);

			Field2 = ExcelUtils.getCellData(i,38);
			driver.findElement(By.name("invd_f2_label")).clear();
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			Thread.sleep(4000);

			Field3 = ExcelUtils.getCellData(i,39);
			driver.findElement(By.name("invd_f3_label")).clear();
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			Thread.sleep(4000);

			Field4 = ExcelUtils.getCellData(i,40);
			driver.findElement(By.name("invd_f4_label")).clear();
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			Thread.sleep(4000);

			Field5 = ExcelUtils.getCellData(i,41);
			driver.findElement(By.name("invd_f5_label")).clear();
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			Thread.sleep(2000);

			Field6 = ExcelUtils.getCellData(i,42);
			driver.findElement(By.name("invd_f6_label")).clear();
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);
			Thread.sleep(3000);	 

			Field7 = ExcelUtils.getCellData(i,43);
			driver.findElement(By.name("invd_f7_label")).clear();
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			Thread.sleep(3000);	

			Field8 = ExcelUtils.getCellData(i,44);
			driver.findElement(By.name("invd_f8_label")).clear();
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			Thread.sleep(3000);	

			Itemname1 = ExcelUtils.getCellData(i,45);
			driver.findElement(By.name("invd_l[1]")).clear();
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			Thread.sleep(3000);

			Itemname2 = ExcelUtils.getCellData(i,46);
			driver.findElement(By.name("invd_l[2]")).clear();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			Thread.sleep(7000);
			//Update
			driver.findElement(By.xpath(" html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			Thread.sleep(7000);
		}
	}


	@Test 
	public  void createdesign_edit_hidden_fields() throws Exception
	{	
		driver.get(Constant.URL);
		System.out.println("--------------------------------");
		System.out.println("createdesign_edit_hidden_fieldsStart");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);
			String symbol = ExcelUtils.getCellData(i,48);
			String IteamDescription = ExcelUtils.getCellData(i,49);
			String amount = ExcelUtils.getCellData(i,50);
			String NetTotal = ExcelUtils.getCellData(i,51);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);

			// edit fields

			symbol = ExcelUtils.getCellData(i,48);
			driver.findElement(By.name("invd_c0_header")).clear();
			driver.findElement(By.name("invd_c0_header")).sendKeys(symbol);
			Thread.sleep(3000);


			IteamDescription = ExcelUtils.getCellData(i,49);
			driver.findElement(By.name("invd_c1_header")).clear();
			driver.findElement(By.name("invd_c1_header")).sendKeys(IteamDescription);
			Thread.sleep(3000);


			amount = ExcelUtils.getCellData(i,50);
			driver.findElement(By.name("invd_c2_header")).clear();
			driver.findElement(By.name("invd_c2_header")).sendKeys(amount);
			Thread.sleep(3000);

			NetTotal = ExcelUtils.getCellData(i,51);
			driver.findElement(By.name("invd_f20_label")).clear();
			driver.findElement(By.name("invd_f20_label")).sendKeys(NetTotal);
			Thread.sleep(6000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");

			Thread.sleep(7000);
		}
	}


	@Test 
	public  void createdesign_delete_iteam1() throws Exception
	{	
		System.out.println("--------------------------------");
		System.out.println("createdesign_delete_iteam1_fields Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);


			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			//edit
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/div[1]/div/a[1]/span")).click();

			//delete iteam1
			driver.findElement(By.xpath(".//*[@id='items-row[1]']/td[1]/a/span")).click();
			Thread.sleep(7000);
			//update
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();

			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(7000);
		}
	}


	@Test 
	public  void createdesign_Quantity() throws Exception
	{	

		System.out.println("--------------------------------");
		System.out.println("createdesign_Quantity  Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,3);
			String Field2 = ExcelUtils.getCellData(i,4);
			String Field3 = ExcelUtils.getCellData(i,5);
			String Field4 = ExcelUtils.getCellData(i,6);
			String Field5 = ExcelUtils.getCellData(i,7);
			String Field6 = ExcelUtils.getCellData(i,8);
			String Field7 = ExcelUtils.getCellData(i,9);
			String Field8 = ExcelUtils.getCellData(i,10);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);
			String Tax = ExcelUtils.getCellData(i,14);
			String servicetax = ExcelUtils.getCellData(i,15);
			String symbol = ExcelUtils.getCellData(i,48);
			String IteamDescription = ExcelUtils.getCellData(i,49);
			String quantity = ExcelUtils.getCellData(i,59);
			String unitprice = ExcelUtils.getCellData(i,60);
			String amount = ExcelUtils.getCellData(i,50);
			String NetTotal = ExcelUtils.getCellData(i,51);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f5_label")).sendKeys(Field5);
			driver.findElement(By.name("invd_f6_label")).sendKeys(Field6);    
			driver.findElement(By.name("invd_f7_label")).sendKeys(Field7);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			driver.findElement(By.name("invd_f18_label")).sendKeys(Tax);
			driver.findElement(By.name("invd_f19_label")).sendKeys(servicetax);
			Thread.sleep(8000);
			// edit fields

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[1]/div[2]/div/div[4]/div[1]/div/a/span")).click();
			driver.findElement(By.xpath(".//*[@id='toggle_quantity_price_column']")).click();

			symbol = ExcelUtils.getCellData(i,48);
			driver.findElement(By.name("invd_c0_header")).clear();
			driver.findElement(By.name("invd_c0_header")).sendKeys(symbol);
			Thread.sleep(3000);


			IteamDescription = ExcelUtils.getCellData(i,49);
			driver.findElement(By.name("invd_c1_header")).clear();
			driver.findElement(By.name("invd_c1_header")).sendKeys(IteamDescription);
			Thread.sleep(3000);

			quantity = ExcelUtils.getCellData(i,59);
			driver.findElement(By.name("invd_c3_header")).clear();
			driver.findElement(By.name("invd_c3_header")).sendKeys(quantity);
			Thread.sleep(3000);

			unitprice = ExcelUtils.getCellData(i,60);
			driver.findElement(By.name("invd_c4_header")).clear();
			driver.findElement(By.name("invd_c4_header")).sendKeys(unitprice);
			Thread.sleep(3000);


			amount = ExcelUtils.getCellData(i,50);
			driver.findElement(By.name("invd_c2_header")).clear();
			driver.findElement(By.name("invd_c2_header")).sendKeys(amount);
			Thread.sleep(3000);

			NetTotal = ExcelUtils.getCellData(i,51);
			driver.findElement(By.name("invd_f20_label")).clear();
			driver.findElement(By.name("invd_f20_label")).sendKeys(NetTotal);
			Thread.sleep(6000);

			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");
			Thread.sleep(7000);
		}
	}


	@Test 
	public  void createdesign_Janacare() throws Exception

	{	
		driver.get(Constant.URL);
		System.out.println("createdesign_janacare Start");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ ) {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_CreateDesignData1,"Sheet1");
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			String expectedTitle = "TraknPay Create Invoice Design";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay create invoice page");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmmss");
			String formattedDate = sdf.format(date);
			designName=ExcelUtils.getCellData(i,1)+formattedDate;
			String Designname = designName;
			String Description = ExcelUtils.getCellData(i,2);
			String Field1 = ExcelUtils.getCellData(i,53);
			String Field2 = ExcelUtils.getCellData(i,54);
			String Field3 = ExcelUtils.getCellData(i,55);
			String Field4 = ExcelUtils.getCellData(i,56);
			String Field8 = ExcelUtils.getCellData(i,57);
			String Itemname1 = ExcelUtils.getCellData(i,11);
			String Itemname2 = ExcelUtils.getCellData(i,12);
			String SubTotal = ExcelUtils.getCellData(i,13);

			//Define implict Wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.id("ctmp_name")).sendKeys(Designname);
			System.out.println("Design name: " +Designname);
			System.out.println("design name created successfully");
			driver.findElement(By.name("ctmp_desc")).sendKeys(Description);
			driver.findElement(By.name("invd_f1_label")).sendKeys(Field1);   
			driver.findElement(By.name("invd_f2_label")).sendKeys(Field2);
			driver.findElement(By.name("invd_f3_label")).sendKeys(Field3);
			driver.findElement(By.name("invd_f4_label")).sendKeys(Field4);
			driver.findElement(By.name("invd_f8_label")).sendKeys(Field8);
			driver.findElement(By.name("invd_l[1]")).sendKeys(Itemname1);
			// To add + 
			driver.findElement(By.xpath(".//*[@id='add_row']/span")).click();
			driver.findElement(By.name("invd_l[2]")).sendKeys(Itemname2);
			driver.findElement(By.name("invd_f17_label")).sendKeys(SubTotal);
			Thread.sleep(7000);
			driver.findElement(By.xpath("html/body/div[2]/div[1]/section[2]/div/form/div[2]/div/button")).click();
			expectedTitle = "TraknPay Show Invoice Design";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay show invoice design page");

			Thread.sleep(7000);
		}
	}

	@Test
	public  void createdesign_sessiontimeout() throws Exception 
	{	
		System.out.println("--------------------------------");
		System.out.println("createdesign session time out");
		System.out.println("--------------------------------");
		for(int i=1 ; i<2; i++ )
		{
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
			System.out.println("loaded: " +i);
			String sUserName = ExcelUtils.getCellData(i,1);
			String sPassword = ExcelUtils.getCellData(i,2);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
			driver.findElement(By.xpath(Xpath.createdesign)).click();
			//start system time
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

			try
			{
				Thread.sleep(16*60*1000);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			date = new Date();
			Thread.sleep(2000);
			System.out.println(dateFormat.format(date)); 
		}		
	}
}


