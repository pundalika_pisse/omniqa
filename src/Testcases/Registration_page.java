
package Testcases;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;

public class Registration_page {

	public static WebDriver	driver = new FirefoxDriver();
	private static WebElement element= null;
	private static String BusinessName;
	private static String Username ;
	private static String EMailAddress;

	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
	//	driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	/*
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	 */
	@Test
	public void Registration() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("-----------------------");
		System.out.println(" Registration Start");
		System.out.println("-----------------------");
		for(int i=1 ; i<2; i++ ) {

			//click for register
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("mmss");
			String formattedDate = sdf.format(date);
			BusinessName =ExcelUtils.getCellData(i,1)+formattedDate;
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			Date date1 = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("mmss");
			String formattedDate1 = sdf1.format(date1);
			Username =ExcelUtils.getCellData(i,2)+formattedDate1;
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(1000);

			Date date11 = new Date();
			SimpleDateFormat sdf11 = new SimpleDateFormat("mmss");
			String formattedDate11 = sdf11.format(date11);
			EMailAddress =ExcelUtils.getCellData(i,3)+formattedDate11;
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(2000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(1000);

			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);

			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(2000);	 

			//register 
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click(); 
			Thread.sleep(4000);
			//close
			driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div[1]/div/div/div/button")).click();

			Thread.sleep(4000);
			// logout
			driver.findElement(By.xpath("html/body/div/header/nav/div/ul/li[3]/a/i")).click();


		}
	}


	@Test
	public void Registration_mandatoryfields_BusinessName() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("-----------------------------------------------");
		System.out.println("Registration_mandatoryfields_Business Name Start");
		System.out.println("--------------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");
			
			/*  
			  String BusinessName= ExcelUtils.getCellData(i,1);
			  driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			  Thread.sleep(1000);
			 */

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);

			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[1]/div/p")).getText().equalsIgnoreCase("The Business Name field is required."))
			{
				System.out.println("pass :The Business Name field is required.");
			}
			else
			{
				System.out.println("Fail:The Business Name field is required.");
			}         
		}
	}


	@Test
	public void Registration_mandatoryfields_Username() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("------------------------------------------");
		System.out.println("Registration_mandatoryfields_Username Start");
		System.out.println("-------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");
			
			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);
			/*		 
			  String Username= ExcelUtils.getCellData(i,2);
			  driver.findElement(By.name("busi_login")).sendKeys(Username);
			  Thread.sleep(4000);
			 */

			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[2]/div/p")).getText().equalsIgnoreCase("The Username field is required."))
			{
				System.out.println("pass:The Username field is required.");
			}
			else
			{
				System.out.println("Fail:The Username field is required.");
			}	           
		}
	}

	@Test
	public void Registration_mandatoryfields_Username_taken() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("------------------------------------------------");
		System.out.println("Registration_mandatoryfields_Username_taken Start");
		System.out.println("-------------------------------------------------");
		for(int i=1 ; i<2; i++ ) 
		{
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");

			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,20);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);


			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();  
			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[2]/div/p")).getText().equalsIgnoreCase("The Username has already been taken."))
			{
				System.out.println("pass :The Username has already been taken.");
			}
			else
			{
				System.out.println("Fail:The Username has already been taken.");
			}	           
		}
	}



	@Test
	public void Registration_mandatoryfields_EMailAddress() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("------------------------------------------------");
		System.out.println("Registration_mandatoryfields_E-Mail Address Start");
		System.out.println("-------------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");
			
			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);

			/*	  
			  String EMailAddress= ExcelUtils.getCellData(i,3);
			  driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
						  Thread.sleep(9000);
			 */
			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[3]/div/p")).getText().equalsIgnoreCase("The E-Mail Address field is required."))
			{
				System.out.println("pass:The E-Mail Address field is required.");
			}
				else
				{
					System.out.println("Fail:The E-Mail Address field is required.");
			}	           
		}
	}


	@Test
	public void Registration_mandatoryfields_phoneNumber() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("---------------------------------------------");
		System.out.println("Registration_mandatoryfields_PhoneNumber Start");
		System.out.println("----------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");

			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);

			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,7);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath(" html/body/div[1]/div[2]/form/div[4]/div/p")).getText().equalsIgnoreCase("The Business Contact Number must be 10 digits."))
			{
				System.out.println("Pass :The Business Contact Number must be 10 digits.");
			}
			else
			{
				System.out.println("Fail:The Business Contact Number must be 10 digits.");
			}	           
		}
	}


	@Test
	public void Registration_mandatoryfields_Password() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("-------------------------------------------");
		System.out.println("Registration_mandatoryfields_Password Start");
		System.out.println("-------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");

			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);

			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);
			/*
		  String Password= ExcelUtils.getCellData(i,5);
		 driver.findElement(By.name("password")).sendKeys(Password);
		 Thread.sleep(6000);
			 */

			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);


			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[5]/div/p")).getText().equalsIgnoreCase("The Password field is required."))
			{
				System.out.println("pass :The Password field is required.");
			}
			else
			{
				System.out.println("Fail:The Password field is required.");
			}	           
		}
	}

	@Test
	public void Registration_mandatoryfields_RetypePassword() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("-------------------------------------------------");
		System.out.println("Registration_mandatoryfields_Retype Password Start");
		System.out.println("--------------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");

			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);


			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);

			/*
		 String RetypePassword= ExcelUtils.getCellData(i,5);
		 driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
		 Thread.sleep(1000);
			 */

			WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);

			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();

			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[5]/div/p")).getText().equalsIgnoreCase("The Password confirmation does not match."))
			{
				System.out.println("pass:The Password confirmation does not match.");
			}
				else
				{
					System.out.println("Fail: The Password confirmation does not match.");
			}	           
		}
	}
	
	@Test
	public void Registration_mandatoryfields_LegalEntity() throws Exception
	{
		driver.get(Constant.URL);
		System.out.println("----------------------------------------------");
		System.out.println("Registration_mandatoryfields_Legal_Entity Start");
		System.out.println("-----------------------------------------------");
		for(int i=1 ; i<2; i++ ) {
			driver.findElement(By.xpath("html/body/div/div[2]/div[2]/a")).click();

			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Registration_1,"Sheet1");

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Traknpay Home");

			String BusinessName= ExcelUtils.getCellData(i,1);
			driver.findElement(By.name("bude_name")).sendKeys(BusinessName);
			Thread.sleep(1000);

			String Username= ExcelUtils.getCellData(i,2);
			driver.findElement(By.name("busi_login")).sendKeys(Username);
			Thread.sleep(4000);


			String EMailAddress= ExcelUtils.getCellData(i,3);
			driver.findElement(By.name("busi_primary_email")).sendKeys(EMailAddress);
			Thread.sleep(9000);

			String MobileNumber= ExcelUtils.getCellData(i,4);
			driver.findElement(By.name("bude_business_contactnum")).sendKeys(MobileNumber);
			Thread.sleep(2000);

			String Password= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password")).sendKeys(Password);
			Thread.sleep(6000);


			String RetypePassword= ExcelUtils.getCellData(i,5);
			driver.findElement(By.name("password_confirmation")).sendKeys(RetypePassword);
			Thread.sleep(1000);

			/*
		 WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='bude_legal_entity']")); 
			Select mySelect= new Select(mySelectElm);
			mySelect.selectByVisibleText("Trust");
			Thread.sleep(9000);
			 */
			//register
			driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[8]/div/button")).click();


			if(driver.findElement(By.xpath("html/body/div[1]/div[2]/form/div[7]/div/p")).getText().equalsIgnoreCase("The Legal Entity field is required."))
			{
				System.out.println("pass:The Legal Entity field is required.");
			}
			else
			{
				System.out.println("Fail:The Legal Entity field is required.");
			}	           
		}
	}
}



