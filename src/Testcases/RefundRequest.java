package Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.Constant;
import Common.ExcelUtils;
import Common.LogIn_Page;
import Common.LogOut_Page;
import Common.Xpath;
import junit.framework.Assert;
import junit.framework.ComparisonFailure;

public class RefundRequest {
	
	
	public static WebDriver	driver = new FirefoxDriver();
//	private static WebElement element = null;
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("URL page");
		//driver = new FirefoxDriver();
		driver.get(Constant.URL);
	}
	
	@AfterMethod
	public void afterTest() {
		try {
			LogOut_Page.logout(driver);
		} catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	
	
	@Test 
	public  void Refund_Filters () throws Exception
 {	
	System.out.println("--------------------------------");
	System.out.println("Refund_Filters Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RefundRequest,"Sheet1");
	 driver.findElement(By.xpath(Xpath.RefundRequest)).click();
	 
     String expectedTitle = "TraknPay List Refund Requests";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay List Refund Requests");
			
			//Define implict Wait           
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	 
	 
	  //Refund_Request_ID
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[1]"));
	  String strng = element.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(strng);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[1]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	 
	//TNP_ID
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element1 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[2]"));
	  String strng1 = element1.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(strng1);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[2]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
		  
	//Invoice_Type
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element2 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]"));
	  String strng2 = element2.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(strng2);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[3]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Customer_Name
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element3 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
	  String strng3 = element3.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(strng3);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[4]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	
	//Customer_Phone 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element4 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[5]"));
	  String strng4 = element4.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(strng4);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[5]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Invoice_Total
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element5 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]"));
	  String strng5 = element5.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(strng5);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[6]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  /*
	  //dropdown 
	  WebElement Refund_Status= driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/select")); 
	  Select mySelect= new Select(Refund_Status);
	  mySelect.selectByVisibleText("All SettlementStatus");
	  Thread.sleep(5000);

	  //dropdown 
	  WebElement Refund_Status1 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/select")); 
	  Select mySelect2= new Select(Refund_Status1);
	  mySelect.selectByVisibleText("completed");
	  Thread.sleep(5000);
*/
	  
	  /* dropdown */
	  WebElement Refund_Status2 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[7]/select")); 
	  Select mySelect3= new Select(Refund_Status2);
	  mySelect3.selectByVisibleText("pending");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[12]/button")).click();
	  
	//Refund_Amount
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element6 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[8]"));
	  String strng6 = element6.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(strng6);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[8]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	  /*
	  //dropdown 
	  WebElement Refund_Initiated= driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/select")); 
	  Select mySelect1= new Select(Refund_Initiated);
	  mySelect.selectByVisibleText("All SettlementStatus");
	  Thread.sleep(5000);

	  //dropdown 
	  WebElement Refund_Initiated1 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/select")); 
	  Select mySelec = new Select(Refund_Initiated);
	  mySelect.selectByVisibleText("completed");
	  Thread.sleep(5000);
	

	  // dropdown 
	  WebElement Refund_Initiated2 = driver.findElement(By.xpath(".//*[@id='filter-row']/th[9]/select")); 
	  Select mySelects = new Select(Refund_Initiated2);
	  mySelects.selectByVisibleText("n");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath(".//*[@id='filter-row']/th[12]/button")).click();
	    */
	  
	//Descriptipon
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element7 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[10]"));
	  String strng7 = element7.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(strng7);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[10]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	  
	//Remarks
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[12]/button")).click();
	  WebElement element8 = driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[11]"));
	  String strng8 = element8.getText();
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(strng8);
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/thead/tr[1]/th[11]/input")).sendKeys(Keys.RETURN);
	  Thread.sleep(3000);
	         	 					}
 					}

	@Test 
	public  void Refund_previous_next() throws Exception
 {	
	System.out.println("-----------------------------------");
	System.out.println("Refund_previous_nextStart");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RefundRequest,"Sheet1");
	 driver.findElement(By.xpath(Xpath.RefundRequest)).click();
     String expectedTitle = "TraknPay List Refund Request ";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay List Refund Request");
 
		 
 /*previous and next*/
    //2 
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_paginate']/ul/li[3]/a")).click();
   Thread.sleep(5000);
// previous
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_previous']/a")).click();
   Thread.sleep(3000);
 //next
   driver.findElement(By.xpath(".//*[@id='dataTableBuilder_next']/a")).click();
   Thread.sleep(4000);
		    
	 			}
 			}


	@Test 
	public  void Refund_show_entries() throws Exception
 {
	System.out.println("-----------------------------------");
	System.out.println("Refund_show_entries Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RefundRequest,"Sheet1");
	 driver.findElement(By.xpath(Xpath.RefundRequest)).click();
     String expectedTitle = "TraknPay List Refund Requests";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay List Refund Requests");

		 
	 //dropdown 
		WebElement mySelectElm = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect= new Select(mySelectElm);
		mySelect.selectByVisibleText("10");
		Thread.sleep(9000);

      //dropdown 
		WebElement mySelectElm2 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect2= new Select(mySelectElm);
		mySelect.selectByVisibleText("25");
		Thread.sleep(9000);

		//dropdown
		WebElement mySelectElm3 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect3= new Select(mySelectElm);
		mySelect.selectByVisibleText("50");
		Thread.sleep(9000);

       // dropdown 
		WebElement mySelectElm4 = driver.findElement(By.xpath(".//*[@id='dataTableBuilder_length']/label/select")); 
		Select mySelect4= new Select(mySelectElm);
		mySelect.selectByVisibleText("100");
		Thread.sleep(9000);
		}
	 }

	
	
	@Test 
	public  void Refund_Edit_Error() throws Exception
 {	
	System.out.println("--------------------------------");
	System.out.println("Refund_Edit_Error Start");
	System.out.println("--------------------------------");
	 for(int i=1 ; i<2; i++ ) {
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_Settlement_login,"Sheet1");
	 System.out.println("loaded: " +i);
	 String sUserName = ExcelUtils.getCellData(i,1);
	 String sPassword = ExcelUtils.getCellData(i,2);
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
	         
	 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_RefundRequest,"Sheet1");
	 driver.findElement(By.xpath(Xpath.RefundRequest)).click();
	 
     String expectedTitle = "TraknPay List Refund Requests";
	 String actualTitle = driver.getTitle();
	 Assert.assertEquals(expectedTitle,actualTitle);
	 System.out.println("Traknpay List Refund Requests");
			
			//Define implict Wait           
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	 
	  driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[12]/a[1]/span")).click();
	   Thread.sleep(4000);
			    
		if (driver.findElement(By.xpath("html/body/div[2]/div/section[2]/div[1]")).getText().equalsIgnoreCase("Invoice with refund pending status cannot be refunded")) 
		{
			System.out.println("pass:Invoice with refund pending status cannot be refunded");
						}
		else
		{
			System.out.println("Fail :Invoice with refund pending status cannot be refunded");	
		}
 		}
 	}

@Test
public  void Refund_sessiontimeout() throws Exception 
{	
	System.out.println("--------------------------------");
	System.out.println("Refund_sessiontimeout start");
	System.out.println("--------------------------------");
	for(int i=1 ; i<2; i++ )
	{
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_LoginData,"Sheet1");
		System.out.println("loaded: " +i);
		String sUserName = ExcelUtils.getCellData(i,1);
		String sPassword = ExcelUtils.getCellData(i,2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LogIn_Page.Execute(sUserName, sPassword,driver,  i, false);
		
		driver.findElement(By.xpath(Xpath.managedesign)).click();
		//start system time
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48

		try
		{
			Thread.sleep(16*60*1000);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		date = new Date();
		Thread.sleep(2000);
		System.out.println(dateFormat.format(date)); 
				}		
			}
	}