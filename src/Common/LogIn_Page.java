package Common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;
import junit.framework.ComparisonFailure;


public class LogIn_Page {

	private static WebElement element = null;
	public static void Execute(String sUserName, String sPassword, WebDriver driver , int cell_row, Boolean checkLogin) throws Exception{


		try{

			String expectedTitle = "TraknPay Home";
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			driver.manage().window().maximize();

			//Enter user name
			driver.findElement(By.name("busi_login")).sendKeys(sUserName);
			//Enter password3
			driver.findElement(By.name("password")).sendKeys(sPassword);
			//Click on login button of biztest
			driver.findElement(By.xpath("html/body/div/div[2]/form/div[3]/button")).click();


			//Check if we are on Dashboard
			expectedTitle = "TraknPay Dashboard";
			actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle,actualTitle);
			System.out.println("Login Successfully"); 



			if(checkLogin){
				ExcelUtils.setCellData("Pass",cell_row, 3, Constant.Path_TestData + Constant.File_LoginData);
			}
		}

		catch (  ComparisonFailure tr)
		{

			System.out.println("Invalid input "+tr);
			System.out.println("Login failed"); 
			driver.findElement(By.name("busi_login")).clear();
			if(checkLogin){
				ExcelUtils.setCellData("Failed",cell_row, 3, Constant.Path_TestData + Constant.File_LoginData);
			}
		}   

	}
}



