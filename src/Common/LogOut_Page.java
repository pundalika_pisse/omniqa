package Common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import junit.framework.Assert;

public class LogOut_Page {

	private static WebElement element = null;

	public static WebElement logout(WebDriver driver) throws Exception{

		driver.findElement(By.xpath("html/body/div/header/nav/div/ul/li[2]/a")).click();
		System.out.println("logout successfull");
		String expectedTitle = "TraknPay Home";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle,actualTitle);
		System.out.println("Traknpay home page");

		return element;

	}
}


